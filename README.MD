# Wikimedia Gadget Scanner

This Python script helps scan gadgets across Wikimedia projects.

## Purpose
The script serves two main purposes:
1. **Identify Third-Party Resource (TPR) Usage:** It scans through JavaScript code of Wikimedia gadgets to identify those that load resources (such as scripts, stylesheets) from third-party domains.
2. **Generate Summary:** Once the scan is complete, it compiles a Markdown table summarizing the gadgets that use third-party resources, listing the project, gadget details, and third-party domains.

## Detection heuristics
### Steps
- **Fetch Gadget lists:** Run SQL queries on Wikimedia project databases to retrieve all gadgets.
- **Obtain Gadget Code:** Use the MediaWiki API to fetch the JavaScript code of gadgets. By default, responses are cached and output is saved `./cache` folder.
- **Strip Comments:** Remove comments from JavaScript code to focus only on executable code.
- **Scan for Third-Party Resources:** Searche for URLs in the gadget code and compare them against an allowlist of Wikimedia domains.
- **Detect Script Loading:** Check for specific JavaScript functions and HTML elements that indicate loading of external scripts or stylesheets.

### Limitations
To avoid the amount of false positives, the pattern detection relies on a combination of two conditions:
- Gadget code must contain some URLs that aren't part of the allow-list
- Gadget code must match one of some abritrary rules such as the presence of `iframe`, `XMLHttpRequest` or `mw.loader`.
This means some gadgets may not be caught if they load third-parties in a way that evades those rules or if they do things like code obfuscation.

## How to Run
### Requirements
* [Python 3.x+](https://www.python.org/downloads/)
* [PIP (Python Dependency Manager)](https://pip.pypa.io/en/stable/installing/)

### Toolforge and dependencies
To run the script on a Toolforge server:
- Log in to Toolforge: `ssh <username>@tools-login.wmflabs.org`
- Create a python virtual environment if none exists: `python3 -m venv venv` 
- Use the virtual environment: `source venv/bin/activate` 
- Install relevant dependencies with `pip install -r requirements.txt`.

### Start the scan
- Run the scan with: `python3 wikimedia-gadgets-loading-tpr.py`
- Bypass default cache `python3 wikimedia-gadgets-loading-tpr.py --no-cache`

