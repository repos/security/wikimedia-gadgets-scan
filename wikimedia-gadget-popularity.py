#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time
from datetime import timedelta
import toolforge
import requests

# Constants: all wikimedia projects dbs, CSP allowlist, SQL query for all gadgets
DB_LIST_URL = 'https://noc.wikimedia.org/conf/dblists/all.dblist'
ALLOWLIST_CSP = [
    'wikimedia.org', 'wikipedia.org', 'wikinews.org',
    'wiktionary.org', 'wikibooks.org', 'wikiversity.org', 'wikisource.org',
    'wikiquote.org', 'wikidata.org', 'wikifunctions.org', 'wikivoyage.org',
    'mediawiki.org',
]
SQL_QUERY = """
    SELECT up_property gadget, COUNT(up_property) as users
    FROM user_properties_anon
    WHERE up_property like "%gadget-%"
    GROUP BY up_property
    ORDER BY users DESC
    LIMIT 1000;
"""


def fetch_gadget_title(project_url: str, page_title: str) -> str:
    end_point = f'https://{project_url}/w/api.php'
    params = {
        'action': 'query',
        'format': 'json',
        'titles': f'MediaWiki:{page_title}',
        'prop': 'info',
        'inprop': 'displaytitle'
    }
    response = requests.get(end_point, params=params)
    pages = response.json().get('query', {}).get('pages', {})
    for page in pages.values():
        gadget_title = page['title'].split(':')[-1]
        return f'{gadget_title}.js'

    return ''


def format_duration(duration_seconds: float) -> str:
    """ Format duration from seconds into hours, minutes, seconds """
    duration = timedelta(seconds=duration_seconds)
    return str(duration)


def print_result(gadgets: [], start_time: float) -> []:
    # Build and print the Markdown table
    output_file = 'gadget-popularity.md'
    output = ''
    output += '| Project | Gadget | Users | URL |\n'
    output += '| ------- | ------ | ------------------- |-----|\n'
    for gadget in gadgets:
        output += f"| {gadget['project']} | {gadget['gadget']} | {gadget['users']} | {gadget['url']}\n"

    with open(output_file, 'w', encoding='utf-8') as f:
        f.write(output)

    print(output + '\n')
    print(f"Markdown table saved to {output_file}")

    end_time = time.time()  # Record the end time of script execution
    duration_seconds = end_time - start_time
    formatted_duration = format_duration(duration_seconds)
    print(f"Script execution completed in {formatted_duration}")


def init():
    """
    Fetch the list of wikis
    """
    start_time = time.time()  # Record the start time of script execution
    wikis = requests.get(DB_LIST_URL).text.split('\n')
    wikis.pop()
    wikis.pop(0)

    results = []
    meta = toolforge.connect('meta')

    for wiki in wikis:
        try:
            conn = toolforge.connect(wiki)
        except Exception as e:
            print(f"Could not connect to {wiki}: {e}")
            continue

        print(f"Getting stats gadgets from {wiki}...")

        data = []
        try:
            with conn.cursor() as cur:
                cur.execute(SQL_QUERY)
                data = cur.fetchall()
        except:
            pass

        if len(data) > 0:
            with meta.cursor() as cur:
                cur.execute('SELECT url FROM wiki WHERE dbname=%s', (wiki,))
                url = cur.fetchall()[0][0]
                project = url.replace('https://', '')

            wiki_gadgets = []

            for row in data:
                raw_title = row[0].decode('utf-8')
                users = row[1]
                gadget_title = fetch_gadget_title(project, raw_title)

                if gadget_title != '':
                    wiki_gadgets.append({
                        'project': project,
                        'gadget': gadget_title,
                        'users': users,
                        'url': f"https://{project}/wiki/MediaWiki:{gadget_title}"
                    })
                # take a 200ms break to avoid API throttling
                time.sleep(.2)
            results.extend(wiki_gadgets)
    print_result(results, start_time)


if __name__ == "__main__":
    init()
