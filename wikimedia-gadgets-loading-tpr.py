#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import hashlib
import os
import time
from datetime import timedelta
import toolforge
import requests
import re

# Constants: all wikimedia projects dbs, CSP allowlist, SQL query for all gadgets
DB_LIST_URL = 'https://noc.wikimedia.org/conf/dblists/all.dblist'
ALLOWLIST_CSP = [
    'wikimedia.org', 'wikipedia.org', 'wikinews.org',
    'wiktionary.org', 'wikibooks.org', 'wikiversity.org', 'wikisource.org',
    'wikiquote.org', 'wikidata.org', 'wikifunctions.org', 'wikivoyage.org',
    'mediawiki.org',
]
SQL_QUERY = """
    SELECT CONCAT('MediaWiki:', page.page_title) AS gadget
    FROM page
    WHERE page.page_namespace = 8
      AND page.page_content_model = 'javascript'
    GROUP BY page.page_title
    ORDER BY page.page_title DESC
    LIMIT 500;
"""
CACHE_FOLDER = 'cache'
CACHE_DISABLED = False


def fetch_gadget_code(project_url: str, page_title: str) -> str:
    """
    Fetch page content using MediaWiki API, with caching
    """
    cache_path = os.path.join(CACHE_FOLDER, get_cache_filename(project_url, page_title))
    gadget_code = retrieve_from_cache(cache_path)
    if gadget_code is not None:
        return gadget_code

    # If not cached, fetch from API
    end_point = f'https://{project_url}/w/api.php'
    params = {
        'action': 'query',
        'format': 'json',
        'titles': page_title,
        'prop': 'revisions',
        'rvprop': 'content'
    }
    response = requests.get(end_point, params=params)
    pages = response.json().get('query', {}).get('pages', {})
    for page in pages.values():
        gadget_code = page.get('revisions', [{}])[0].get('*', '')
        save_to_cache(cache_path, gadget_code)
        return gadget_code


def extract_domain(url: str) -> str:
    """
    Match and extract domain or subdomain from the URL
    """
    match = re.match(r'(?:https?://)?([^/]+)', url)
    if match:
        return match.group(1)
    return ''


def strip_comments(code: str) -> str:
    """
    Remove comments from the JS code.
    """
    comment_pattern = re.compile(r'(?<!http:)//.*?$|/\*.*?\*/', re.DOTALL | re.MULTILINE)
    return re.sub(comment_pattern, '', code)


def scan_for_third_party(content: str) -> []:
    """
    Try to get a comma-separated list of TPRs
    """
    url_pattern = re.compile(r'https?://([^\s\'"<>]+)/?')
    content = strip_comments(content)
    urls = url_pattern.findall(content)
    third_parties = set()

    # Detect the existence of any TPR in code
    for url in urls:
        extracted_domain = extract_domain(url)
        is_allowed_domain = False
        if extracted_domain != '':
            for allowlist_domain in ALLOWLIST_CSP:
                if allowlist_domain in extracted_domain:
                    is_allowed_domain = True
                    break
            if not is_allowed_domain:
                third_parties.add(extracted_domain)

    return third_parties


def has_script_loading(code) -> bool:
    """
    Detect whether some script happening
    """
    rules = [
        r'mw\.loader\b',  # Match mw.loader
        r'import\b',  # Match import (import statements)
        r'importScript\b',  # Match importScript
        r'XMLHttpRequest\b',  # Match XMLHttpRequest
        r'jquery\.load\b',  # Match jquery.load
        r'fetch\b',  # Match fetch API
        r'iframe\b',  # Match iframe tags
        r'form\b',  # Match form tags
        r'rel=["\']stylesheet["\']',  # Match external CSS links
        r'@import\b',  # Match CSS @import rules
        r'\$.getScript\b',  # Match jQuery getScript
        r'<script\b[^>]*\bsrc=["\']',  # Match <script> tags with a src attribute
    ]

    compiled_rules = [re.compile(rule) for rule in rules]
    for pattern in compiled_rules:
        if pattern.search(code):
            return True

    return False


# Function to retrieve gadget code from cache
def retrieve_from_cache(cache_path: str):
    """ Retrieve gadget code from cache file """
    if CACHE_DISABLED or not os.path.exists(cache_path):
        return None

    with open(cache_path, 'r', encoding='utf-8') as f:
        return f.read()


# Function to save gadget code to cache
def save_to_cache(cache_path: str, gadget_code: str):
    """ Save gadget code to cache file """
    if CACHE_DISABLED:
        return

    cache_folder = os.path.dirname(cache_path)
    if not os.path.exists(cache_folder):
        os.makedirs(cache_folder)
    with open(cache_path, 'w', encoding='utf-8') as f:
        f.write(gadget_code)


# Function to generate cache filename based on project_url and page_title
def get_cache_filename(project_url: str, page_title: str) -> str:
    # Combine project_url and page_title to create a unique cache filename
    combined_str = f"{project_url}-{page_title}"
    hash_str = hashlib.md5(combined_str.encode('utf-8')).hexdigest()
    return f"{hash_str}.js"


def format_duration(duration_seconds: float) -> str:
    """ Format duration from seconds into hours, minutes, seconds """
    duration = timedelta(seconds=duration_seconds)
    return str(duration)


def print_result(gadgets: [], start_time: float) -> []:
    # Build and print the Markdown table
    output_file = 'gadget-list.md'
    output = ''
    output += '| Project | Gadget | Third-Party Domains | URL |\n'
    output += '| ------- | ------ | ------------------- |-----|\n'
    for gadget in gadgets:
        output += f"| {gadget['project']} | {gadget['gadget']} | {gadget['third_party_domains']} | {gadget['url']}\n"

    with open(output_file, 'w', encoding='utf-8') as f:
        f.write(output)

    print(output + '\n')
    print(f"Markdown table saved to {output_file}")

    end_time = time.time()  # Record the end time of script execution
    duration_seconds = end_time - start_time
    formatted_duration = format_duration(duration_seconds)
    print(f"Script execution completed in {formatted_duration}")


def init(no_cache: bool = False):
    """
    Fetch the list of wikis
    """
    CACHE_DISABLED = no_cache
    start_time = time.time()  # Record the start time of script execution
    wikis = requests.get(DB_LIST_URL).text.split('\n')
    wikis.pop()
    wikis.pop(0)

    results = []
    meta = toolforge.connect('meta')

    for wiki in wikis:
        try:
            conn = toolforge.connect(wiki)
        except Exception as e:
            print(f"Could not connect to {wiki}: {e}")
            continue

        print(f"Scanning gadgets on {wiki}...")

        with conn.cursor() as cur:
            cur.execute(SQL_QUERY)
            data = cur.fetchall()

        if len(data) > 0:
            with meta.cursor() as cur:
                cur.execute('SELECT url FROM wiki WHERE dbname=%s', (wiki,))
                url = cur.fetchall()[0][0]
                project = url.replace('https://', '')

            wiki_gadgets = []

            for row in data:
                gadget_title = row[0].decode('utf-8')
                gadget_code = fetch_gadget_code(project, gadget_title)
                third_party_domains = scan_for_third_party(gadget_code)

                # If code has TPRs but doesn't do any script loading, discard it
                # if not has_script_loading(gadget_code):
                #    third_party_domains = []

                if third_party_domains:
                    wiki_gadgets.append({
                        'project': project,
                        'gadget': gadget_title,
                        'third_party_domains': ', '.join(third_party_domains),
                        'url': f"https://{project}/wiki/{gadget_title}"
                    })

            if wiki_gadgets:
                print(f"Summary for {wiki}:")
                for gadget_info in wiki_gadgets:
                    print(f"- Gadget: {gadget_info['gadget']}")
                    print(f"  Third-party domains: {gadget_info['third_party_domains']}")
                results.extend(wiki_gadgets)
            else:
                print(f"No gadgets with third-party domains found for {wiki}")

    print_result(results, start_time)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Fetch and scan MediaWiki gadgets.")
    parser.add_argument('--no-cache', action='store_true', help="Bypass cache and fetch from API")
    args = parser.parse_args()

    init(args.no_cache)
